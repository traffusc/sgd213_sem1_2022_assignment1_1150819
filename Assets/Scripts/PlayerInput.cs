using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{

    private Movement playerMovement;
    private Shoot playerShoot;

    void Start()
    {
        playerMovement = GetComponent<Movement>();
        playerShoot = GetComponent<Shoot>();
    }

    void Update()
    {
        //Move the player
        float horizontalInput = Input.GetAxis("Horizontal");

        if (horizontalInput != 0.0f && playerMovement)
        {
            playerMovement.Move(horizontalInput, 0f);
        }

        if (Input.GetButton("Fire1") && playerShoot != null)
        {
            playerShoot.Fire();
        }
    }
}

