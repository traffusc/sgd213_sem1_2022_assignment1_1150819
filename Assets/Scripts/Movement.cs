﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private float speed = 5000f;
    private Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    //Standardised movement script for x,y input
    public void Move(float horizontalInput, float verticalInput)
    {
        //Normalize vector to ensure speed is consistent
        Vector2 ForceToAdd = new Vector2(horizontalInput, verticalInput).normalized * speed * Time.deltaTime;
        rb2d.AddForce(ForceToAdd);
    }
}