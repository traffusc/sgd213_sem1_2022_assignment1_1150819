﻿using UnityEngine;
using System.Collections;

public class SpawnOverTime : MonoBehaviour
{

    [SerializeField]
    private GameObject spawnObject;

    [SerializeField]
    private float spawnDelay = 2f;
    private Renderer ourRenderer;
    private float xBoundsLeft;
    private float xBoundsRight;

    void Start()
    {
        ourRenderer = GetComponent<Renderer>();

        //Stop our Spawner from being visible!
        ourRenderer.enabled = false;

        //Spawner doesn't move - calculate bounds just once.
        float ourRendererModifierX = ourRenderer.bounds.size.x / 2;
        xBoundsLeft = transform.position.x - ourRendererModifierX;
        xBoundsRight = transform.position.x + ourRendererModifierX;

        InvokeRepeating("Spawn", spawnDelay, spawnDelay);
    }

    void Spawn()
    {

        Vector2 spawnPoint = new Vector2(Random.Range(xBoundsLeft, xBoundsRight), transform.position.y);

        Instantiate(spawnObject, spawnPoint, Quaternion.identity);
    }
}
