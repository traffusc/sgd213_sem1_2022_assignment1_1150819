﻿using UnityEngine;
using System.Collections;

public class BulletMoveForward : MonoBehaviour
{
    //expose properties for different bullet creation/speeds/accelerations
    [SerializeField]
    private float initialVelocity = 5f;
    private Rigidbody2D rb2d;
    private Movement projectileMovement;

    void Start()
    {
        projectileMovement = GetComponent<Movement>();
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.up * initialVelocity;
    }

    void Update()
    {
        //Exit out if Movement not found.
        if (!projectileMovement)
        {
            return;
        }
        
        /*Hardcoded for bullets always moving up. Could be improved similarly for enemies by providing x values 
        for new bullet types/shooting styles - but out of scope/overly complicating the requirement.
        */
        projectileMovement.Move(0f, 1f);
    }
}
