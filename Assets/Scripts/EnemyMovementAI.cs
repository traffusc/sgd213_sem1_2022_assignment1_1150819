﻿using UnityEngine;
using System.Collections;

public class EnemyMovementAI : MonoBehaviour
{
    //Expose parameters for different enemy type easy modification.
    [SerializeField]
    private float initialVelocity = 2f;
    [SerializeField, Range(-1f, 1f)]
    private float xMovementDirection = 0;
    [SerializeField, Range(-1f, 1f)]
    private float yMovementDirection = -1;
    private Rigidbody2D rb2d;
    private Movement enemyMovement;

    void Start()
    {
        enemyMovement = GetComponent<Movement>();
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.down * initialVelocity;
    }

    void Update()
    {
        //Exit out if Movement not found.
        if (!enemyMovement)
        {
            return;
        }
        enemyMovement.Move(xMovementDirection, yMovementDirection);
    }
}
